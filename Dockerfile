FROM golang:1 AS build

WORKDIR /code

# Tools
RUN curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | bash -s -- -b "$GOPATH/bin" v1.12.2 \
 && go get -u github.com/kyoh86/richgo
# Go 1.11+ modules
ENV GO111MODULE=on

# Deps
COPY go.* ./
RUN go mod download

# Copy in the rest of the code so as to not disturb the dependency layer cache
COPY *.go ./

# Test, lint, and compile the code
RUN richgo test \
    -v \
    -bench=. \
    . \
 && golangci-lint run \
    --deadline '2m' \
    --enable-all \
    --disable gochecknoglobals,gochecknoinits,scopelint,megacheck \
 && CGO_ENABLED=0 GOOS=linux GOARCH=386 go build \
    -a -installsuffix cgo -ldflags='-w -s' -o /binary -v \
    .

FROM scratch AS final

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=build /binary /binary

USER 1000
ENTRYPOINT ["/binary"]
