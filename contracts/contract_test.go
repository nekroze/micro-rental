package contracts

import (
	"net/http"
	"testing"
	"time"

	. "github.com/JumboInteractiveLimited/Gandalf"
	"github.com/JumboInteractiveLimited/Gandalf/check"
	"github.com/JumboInteractiveLimited/Gandalf/pathing"
)

var quoteContractCheck = pathing.GJSONChecks(pathing.PathChecks{
	"name":     check.RegexMatch(`.+`),
	"amount":   check.RegexMatch(`[0-9]+\.[0-9]+`),
	"currency": check.RegexMatch(`.+`),
})

func statelessContracts() []*Contract {
	return []*Contract{
		{Name: "Quotes",
			Request: NewSimpleRequester("GET", "http://provider/quotes", "", nil, time.Second*5),
			Check: &SimpleChecker{
				HTTPStatus: 200,
				Headers: http.Header{
					"Content-Type":                []string{"application/json"},
					"Access-Control-Allow-Origin": []string{"*"},
				},
				ExampleBody: `{"offerings":{"RentalCompanyB":[{"amount":"42.00","currency":"AUD","name":"Rent A Rental"}]}}`,
				BodyCheck: pathing.GJSONChecks(pathing.PathChecks{
					"offerings.RentalCompanyB": quoteContractCheck,
					"offerings.*":              quoteContractCheck,
				}),
			},
			Export: &ToMMock{},
		},
	}
}

func TestStatelessContracts(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}
	for _, tc := range statelessContracts() {
		t.Run(tc.Name, func(st *testing.T) {
			tc.Assert(st)
		})
	}
}

func BenchmarkStatelessContracts(b *testing.B) {
	if testing.Short() {
		b.Skip()
	}
	for _, bc := range statelessContracts() {
		b.Run(bc.Name, func(sb *testing.B) {
			bc.Benchmark(sb)
		})
	}
}

func TestMain(m *testing.M) {
	Main(m)
}
