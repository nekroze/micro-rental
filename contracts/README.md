# Contracts

This is a self contained repository of contracts for the various services in the Micro Rental public API.

# Building

Contracts are stored in an immutable container allowing them to be deployed to and from anywhere to run against any target. Any changes to the contracts require a rebuild of the image(s) done like so:

```bash
docker-compose build --force-rm
```

# Testing

These contracts can be used to generate a mock implementation of the `provider` service while self testing the contracts for consistency via the following commands:

```bash
docker-compose up
```

If you would only like to test the contracts then use the following commands:

```bash
docker-compose run --rm contracts
```
