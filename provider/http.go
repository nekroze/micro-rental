package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/rs/zerolog/log"

	"github.com/julienschmidt/httprouter"
)

func getQuotesBlob() ([]byte, error) {
	quotes, err := getAllQuotes()
	if err != nil {
		return nil, err
	}

	return json.Marshal(quotes)
}

func quotes(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	log.Info().
		Str("client", r.RemoteAddr).
		Msg("recieved request for quotes")

	blob, err := getQuotesBlob()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Error().
			Err(err).
			Msg("Failed to generate a body of quotes")
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	fmt.Fprintf(w, string(blob))
}

func newRouter() *httprouter.Router {
	router := httprouter.New()

	router.GET("/quotes", quotes)

	return router
}

func runServer() error {
	address := getListenAddress()

	log.Info().
		Str("address", address).
		Msg("Starting http server")

	return http.ListenAndServe(address, newRouter())
}
