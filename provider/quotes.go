package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"
)

// In a more realistic scenario this struct might be part of a shared library,
// the replication of which is out of scope of this project.
type Quote struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
	Name     string `json:"name"`
}

type Quotes struct {
	Offerings map[string][]Quote `json:"offerings"`
}

var httpc = &http.Client{
	Timeout: time.Second,
}

func getAllQuotes() (quotes Quotes, err error) {
	quotes.Offerings = map[string][]Quote{}
	for company, service := range backends {
		var resp *http.Response
		var respBytes []byte
		var respData []Quote

		resp, err = httpc.Get("http://" + service)
		if err != nil {
			break
		}

		respBytes, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			break
		}

		err = json.Unmarshal(respBytes, &respData)
		if err != nil {
			break
		}
		quotes.Offerings[company] = respData
	}
	if len(quotes.Offerings) > 0 && err != nil {
		err = nil
	}
	return quotes, err
}
