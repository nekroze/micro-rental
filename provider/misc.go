package main

import (
	"fmt"
	"os"
)

func getEnvOrDefault(variableName, defaultValue string) string {
	if value, ok := os.LookupEnv(variableName); ok {
		return value
	}
	return defaultValue
}

func getListenAddress() string {
	return fmt.Sprintf(
		"%s:%s",
		getEnvOrDefault("LISTEN_ADDRESS", ""),
		getEnvOrDefault("LISTEN_PORT", "8080"),
	)
}
