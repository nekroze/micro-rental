package main

import (
	"net/http"
	"testing"
	"time"

	. "github.com/JumboInteractiveLimited/Gandalf"
)

func statelessContracts() []*Contract {
	return []*Contract{
		{Name: "Quotes",
			Request: NewSimpleRequester("GET", "http://provider/quotes", "", nil, time.Second*5),
			Check: &SimpleChecker{
				HTTPStatus: 200,
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
				ExampleBody: `{"offerings":{"FauxRentals":[{"amount":"57.00","currency":"AUD","name":"Rapid Rental"}]}}`,
			},
		},
	}
}

func TestStatelessContracts(t *testing.T) {
	for _, tc := range statelessContracts() {
		t.Run(tc.Name, func(st *testing.T) {
			tc.Assert(st)
		})
	}
}

func BenchmarkStatelessContracts(b *testing.B) {
	for _, bc := range statelessContracts() {
		b.Run(bc.Name, func(sb *testing.B) {
			bc.Benchmark(sb)
		})
	}
}

func TestMain(m *testing.M) {
	UseFakeBackend()
	MainWithHandler(m, newRouter())
}
