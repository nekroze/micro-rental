package main

import (
	"reflect"
	"testing"
)

func Test_getAllQuotes(t *testing.T) {
	tests := []struct {
		name       string
		wantQuotes Quotes
		wantErr    bool
	}{
		{
			name: "HappyPath",
			wantQuotes: Quotes{
				Offerings: map[string][]Quote{
					"FauxRentals": {
						{
							Name:     "Rapid Rental",
							Amount:   "57.00",
							Currency: "AUD",
						},
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotQuotes, err := getAllQuotes()
			if (err != nil) != tt.wantErr {
				t.Errorf("getAllQuotes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotQuotes, tt.wantQuotes) {
				t.Errorf("getAllQuotes() = %v, want %v", gotQuotes, tt.wantQuotes)
			}
		})
	}
}
