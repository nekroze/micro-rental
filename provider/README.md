# Configuration

For maximum immutability configuration is built into a data structure stored at
the top of `backends.go` as a map of company names we support to the backend
service that provides quotes for that companies service discovery tag.
