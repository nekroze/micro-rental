package main

import "github.com/rs/zerolog/log"

func main() {
	if err := runServer(); err != nil {
		log.Fatal().
			Err(err).
			Msg("http server failure")
	}
}
