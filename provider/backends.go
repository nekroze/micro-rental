package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
)

// Maps company backends we support to service discovery (provided by docker in
// this project) names that can be requested over the network.
var backends = map[string]string{
	"RentalCompanyA": "backend_rca:8080",
	"RentalCompanyB": "backend_rcb:8080",
}

func UseFakeBackend() {
	// Setup a dummy backend server for testing over the loopback interface
	router := httprouter.New()
	router.GET(
		"/test/backend",
		func(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
			fmt.Fprintf(w, `[{"amount":"57.00","currency":"AUD","name":"Rapid Rental"}]`)
		},
	)

	// Start it up and use it
	go func() {
		log.Fatal().Err(http.ListenAndServe(":9999", router)).Msg("http test server failure")
	}()
	backends = map[string]string{
		"FauxRentals": "localhost:9999/test/backend",
	}
	time.Sleep(time.Second)
}
