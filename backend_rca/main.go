package main

import (
	"github.com/rs/zerolog/log"
)

func initResponse() {
	log.Info().
		Msg("Initializing response")

	var err error
	response, err = buildResponse()
	if err != nil {
		panic(err)
	}
}

func main() {
	initResponse()

	startPolling()

	if err := runServer(); err != nil {
		log.Fatal().
			Err(err).
			Msg("http server failure")
	}
}
