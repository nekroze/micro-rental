package main

import (
	"reflect"
	"testing"
)

func Test_pollQuotes(t *testing.T) {
	tests := []struct {
		name       string
		wantQuotes []Quote
		wantErr    bool
	}{
		{
			name: "HappyPath",
			wantQuotes: []Quote{
				{
					Name:     "Rapid Rental",
					Amount:   "58.00",
					Currency: "AUD",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotQuotes, err := pollQuotes()
			if (err != nil) != tt.wantErr {
				t.Errorf("pollQuotes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotQuotes, tt.wantQuotes) {
				t.Errorf("pollQuotes() = %v, want %v", gotQuotes, tt.wantQuotes)
			}
		})
	}
}
