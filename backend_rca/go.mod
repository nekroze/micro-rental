module gitlab.com/Nekroze/microrental/backend_rca

require (
	github.com/JumboInteractiveLimited/Gandalf v0.0.0-20180614042730-b6fb46388ba0
	github.com/JumboInteractiveLimited/jsonpath v0.0.0-20180321012328-6fcdcc9066b5 // indirect
	github.com/eapache/go-resiliency v1.1.0 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/jmartin82/mmock v2.4.3+incompatible // indirect
	github.com/julienschmidt/httprouter v1.2.0
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/radovskyb/watcher v1.0.5 // indirect
	github.com/rs/zerolog v1.11.0
	github.com/tidwall/gjson v1.1.4 // indirect
	github.com/tidwall/match v1.0.1 // indirect
)
