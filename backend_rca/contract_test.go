package main

import (
	"net/http"
	"testing"
	"time"

	. "github.com/JumboInteractiveLimited/Gandalf"
	"github.com/JumboInteractiveLimited/Gandalf/check"
	"github.com/JumboInteractiveLimited/Gandalf/pathing"
)

func statelessContracts() []*Contract {
	return []*Contract{
		{Name: "Quote",
			Request: NewSimpleRequester("GET", "http://provider/", "", nil, time.Second),
			Check: &SimpleChecker{
				HTTPStatus: 200,
				Headers: http.Header{
					"Content-Type": []string{"application/json"},
				},
				ExampleBody: `[{"name":"Service A Rental","amount":"55.00","currency":"AUD"}]`,
				BodyCheck: pathing.GJSONChecks(pathing.PathChecks{
					"#.name":     check.RegexMatch(`.+`),
					"#.amount":   check.RegexMatch(`[0-9]+\.[0-9]+`),
					"#.currency": check.RegexMatch(`.+`),
				}),
			},
		},
	}
}

func TestStatelessContracts(t *testing.T) {
	for _, tc := range statelessContracts() {
		t.Run(tc.Name, func(st *testing.T) {
			tc.Assert(st)
		})
	}
}

func BenchmarkStatelessContracts(b *testing.B) {
	for _, bc := range statelessContracts() {
		b.Run(bc.Name, func(sb *testing.B) {
			bc.Benchmark(sb)
		})
	}
}

func TestMain(m *testing.M) {
	UseFakeUpstream()
	initResponse()
	MainWithHandler(m, newRouter())
}
