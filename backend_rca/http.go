package main

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
)

func quote(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	log.Info().
		Str("client", r.RemoteAddr).
		Msg("recieved request for quote")

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, response)
}

func newRouter() *httprouter.Router {
	router := httprouter.New()

	router.GET("/", quote)

	return router
}

func runServer() error {
	address := getListenAddress()

	log.Info().
		Str("address", address).
		Msg("Starting http server")

	return http.ListenAndServe(address, newRouter())
}
