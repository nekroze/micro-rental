package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/rs/zerolog/log"
)

var httpc = &http.Client{
	Timeout: time.Second * 30,
}

var upstream = getEnvOrDefault("UPSTREAM_URL", "https://api.myjson.com/bins/7c0qw")

type upstreamResponse struct {
	Quote struct {
		Amount   string `json:"amount"`
		Currency string `json:"currency"`
		Name     string `json:"name"`
	} `json:"quote"`
}

func pollQuotes() (quotes []Quote, err error) {
	resp, err := httpc.Get(upstream)
	if err != nil {
		log.Error().
			Err(err).
			Str("upstream", upstream).
			Msg("Failed to poll upstream")
		return quotes, err
	}

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return quotes, err
	}

	var respData upstreamResponse

	if e := json.Unmarshal(respBytes, &respData); e != nil {
		return quotes, err
	}

	quotes = append(quotes, respData.Quote)

	return quotes, err
}

// For testing purposes, setup a dummy upstream server for testing over the loopback interface.
func UseFakeUpstream() {
	router := httprouter.New()
	router.GET(
		"/test/upstream",
		func(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
			fmt.Fprintf(w, `{"quote":{"amount":"58.00","currency":"AUD","name":"Rapid Rental"}}`)
		},
	)

	go func() {
		if e := http.ListenAndServe(":9999", router); e != nil {
			log.Fatal().
				Err(e).
				Msg("http test server failure")
		}
	}()
	upstream = "http://localhost:9999/test/upstream"
	time.Sleep(time.Second)
}
