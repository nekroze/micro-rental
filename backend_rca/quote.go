package main

import (
	"encoding/json"
	"time"

	"github.com/rs/zerolog/log"
)

// In a more realistic scenario this struct might be part of a shared library,
// the replication of which is out of scope of this project.
type Quote struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
	Name     string `json:"name"`
}

var response string

func buildResponse() (string, error) {
	data, err := pollQuotes()
	if err != nil {
		return "", err
	}

	blob, err := json.Marshal(data)
	if err != nil {
		return "", err
	}
	return string(blob), nil
}

const pollingInterval = 5 * time.Second

func startPolling() {
	log.Info().
		Str("upstream", upstream).
		Str("interval", pollingInterval.String()).
		Msg("Initializing polling")

	ticker := time.NewTicker(pollingInterval)
	go func() {
		for range ticker.C {
			newResponse, err := buildResponse()
			if err != nil {
				log.Error().
					Err(err).
					Msg("Failed to get data from upstream")
			} else {
				response = newResponse
			}
		}
	}()
}
