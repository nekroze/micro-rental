package main

import (
	"io/ioutil"

	"github.com/rs/zerolog/log"
)

func init() {

	log.Logger = log.Output(ioutil.Discard)
}
