package main

import "encoding/json"

// In a more realistic scenario this struct might be part of a shared library,
// the replication of which is out of scope of this project.
type Quote struct {
	Amount   string `json:"amount"`
	Currency string `json:"currency"`
	Name     string `json:"name"`
}

var data = []Quote{
	{
		Name:     "Rapid Rental",
		Amount:   "57.00",
		Currency: "AUD",
	},
}

var response = func() string {
	blob, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}
	return string(blob)
}()
