# Micro Rental

An exploration in microservices. Provides a public facing API serving an
aggregate of quotes provided by various backend systems.

# Design

At the lowest layer are the backends. For each company we can provide quotes for
we have a unique service. Currently supporting quotes from RentalCompanyA and
RentalCompanyB are the services `backend_rca` and `backend_rcb` respectively.

These backends provide an HTTP API upon which they solely serve and respond
with their respective companies offerings.

RentalCompanyB has a static offering whereas RentalCompanyA provide their own
HTTP API on the internet that must be queried for quotes. Due to this the 
`backend_rcb` service uses a mini form of the Materialized View pattern in that
data is sourced from an external input and an eventually consistent view of
that source is constructed in memory. This means the differences in performance
between `backend_rca` and `backend_rcb` are negligible even though one relies
on data from the internet.

The `provider` services frontend public interface that responds with data
aggregated from all supported backends by calling out to them over HTTP.

# Running

Execution requires Go at a minimum or alternatively this project is setup for
orchestration via docker and docker-compose.

Alternatively, if you do not have/want docker you can run everything entirely
in the cloud via your browser via [Play With Docker][1] which will drop you
into a terminal with everything you need to run all commands in this document
after you `cd` into the freshly cloned `micro-rental` directory.

## Production Like Environment

To setup something to use or demo as an approximation of a production cluster
simply execute the following:

```bash
docker-compose up provider backend_rca backend_rcb
```

We could just run provider and all dependencies would be started as required
but we specify the backends here so we can see the logs.

Once running it will bind itself to your localhost port 8080 such that you can
now get quotes at:

http://localhost:8080/quotes

## Development

All services are deployed in immutable docker containers running a single
process under an unprivileged user. Due to this and the process being written
in a compiled language (Go) any changes require a rebuild of the image(s) like
so, optionally doing a single one by appending its service name:

```bash
docker-compose build --force-rm
```

All go code has its internal (unit) tests executed and its code checked for
correctness and quality at time of build and all dependencies are version
locked via go 1.11 modules. Due to this it is impossible to create a wildly
broken image.

## Cleanup

To remove all docker artifacts created by this project execute this command:

```bash
docker-compose down --volumes --rmi all --remove-orphans
```

## Testing

First start a [Production Like Environment][2] in a
different shell (or append the `--detach` switch when upping the provider
service) then you use the following defined services to test Micro Rental.

### Get a response like an end user

You can use the `quotes` service to do a single request to the quotes service
at any time and get the same output an end user of the service would:

```bash
docker-compose up quotes
```

### Execute integration contract tests

You can test that the cluster of services are all talking to each other by
executing the contract tests like so:

```bash
docker-compose up contracts
```

### Execute benchmarks

All tests of contracts, either at docker build time or via the `contracts`
service also execute a short benchmark run however for testing more extreme and
concurrent load scenarios its hard to say no to Apache Bench which is
conveniently wrapped in the `bench` service:

```bash
docker-compose up bench
```

### Testing sandbox for B2B users

When building services that integrate with the Micro Rental public API it is
often useful to provide a mock of the API with fake data for testing purposes.
This is provided by MMock:

```bash
docker-compose up mock contracts
```

The contract tests will execute once in order to construct the mock definitions
via the very contracts that test the real public API for correctness. Once
complete you will now be running a mock on localhost port 8081 available like
this:

http://localhost:8081/quotes

[1]: http://play-with-docker.com/?stack=https://gitlab.com/nekroze/micro-rental/raw/master/pwd.yml
[2]: #ProductionLikeEnvironment
